import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

public class ACZeichenKetteEinlesen {

    private static Scanner scan;

    /**
     * Lese Zeichenkette ein
     *
     * @param STR Zeichenkette
     */
    public void zeichenKetteEinlesen(final String STR) {
        scan = new Scanner(System.in);
        boolean gueltig = false;
        String input;

        while (!gueltig) {
            System.out.println(STR);
            input = scan.nextLine().replaceAll("\\s+", ""); // Entfernt alle Leerzeichen, tabulatoren usw.
            gueltig = eingabePruefen(input);
            if (gueltig)
                gueltig = arrayListFuellen(input);
            if (gueltig) {
                spannungsQuelleEinlesen("Bitte den Wert für die Spannungsquelle in Volt eingeben:");

                WechselStromKreis.gesamtZ();
                WechselStromKreis.berechne();
            }
        }
    }

    /**
     * Hier wird die Zeichenkette geprüft, falls mehr oder weniger schließenden Klammer als Öffnenden
     * Außerdem wird geprüft, ob die Zeichenkette Buchstaben enthält und ob zwischen den Klammer '+' statt '/' eingegeben wurde
     *
     * @param INPUT Zeichenkette
     */
    public boolean eingabePruefen(final String INPUT) {

        String errorMsg = "";

        Stack<Character> klammerStack = new Stack<>();
        char current, top;
        boolean gueltig = true; // Setze die Gueltigkeits-Variable erstmal auf true

        // durch Zeichen des Strings iterieren
        for (int i = 0; i < INPUT.length(); i++) {
            current = INPUT.charAt(i);

            if (Character.isLetter(current)) {
                if (current == 'R' || current == 'r' || current == 'L' || current == 'l' || current == 'C' || current == 'c') {
                    continue;
                } else {
                    errorMsg = "Als Buchstaben darf nur 'R', 'L' oder 'C' eingegeben werden.";
                    gueltig = false;
                    break;
                }
            }

            if (Character.isDigit(current)) continue;

            if (current == '+' && !klammerStack.isEmpty() && klammerStack.lastElement() == '(') {
                errorMsg = "Es soll kein '+' zwischen den Klammern eingegeben werden.";
                gueltig = false;
            }

            if (current == '/' && klammerStack.isEmpty()) {
                errorMsg = "Fehler, Parallelwiderstände sollen zwischen zwei Klammern eingegeben werden.";
                gueltig = false;
            }

            if (current == '(') {
                if (!klammerStack.isEmpty()) {
                    errorMsg = "Fehler, Keine weiteren Klammern in gepaarten Klammern erlaubt.";
                    gueltig = false;
                }
                klammerStack.push(current);
            } else if (current == ')') {
                //Fall 1: mehr schliessende Klammern als oeffnende.
                //Falls eine schliessende Klammer vorhanden ist, obwohl der Stack leer ist, ist die Zeichenkette ungültig
                if (klammerStack.isEmpty()) {
                    errorMsg = "Fehler: Mehr schliessende Klammern als oeffnende.";
                    gueltig = false;
                } else {
                    // oberste Klammer vom Stack nehmen
                    top = klammerStack.pop();

                    //Fall 2: Falls die aktuelle Klammer nicht zur obersten Klammer auf dem Stack passt, ist die Zeichenkette ungültig
                    if (top != '(') {
                        errorMsg = "Fehler: Die aktuelle schliessende Klammer passt\nnicht zur obersten Klammer auf dem Stack.";
                        gueltig = false;
                    }
                }
            }
        }


        //Fall 3: mehr oeffnende Klammern als Schliessende.
        //Falls keine schliessenden Klammern vorhanden sind, obwohl der Stack nicht leer ist, ist die Zeichenkette ungültig
        if (!klammerStack.isEmpty()) {
            errorMsg = "Fehler: Es sind keine weiteren schliessenden Klammern vorhanden.";
            gueltig = false;
        }

        /*
         * Hier werden 6 Arten von Fehleingaben durch reguläre Ausdrücke ausgeschlossen
         * 1. Keine Zahl nach der schliessenden Klammer.                                     z.B. (2+3)3.
         * 2. Kein Operator oder schliessende Klammer nach einem Operator.                   z.B. (2++3), (2+).
         * 3. Kein Operator nach oeffnenden Klammer.                                         z.B. (+2+3).
         * 4. Keine Zahl oder schliessende/oeffnende Klammer vor oeffnenden Klammer.         z.B. 3(2+6), )(2+3).
         * 5. Die Zeichenkette muss mit einer oeffnenden Klammer oder einer Zahl beginnen.   z.B. +8+3
         * 6. muss mit einer Zahl oder einer schliessenden Klammer enden.                    z.B. 3+6+
         */
        if (gueltig) {
            String regex = "(.*\\)\\d.*)|" + "(.*[+/]([+/]|\\)).*)|" + "(.*\\([+/].*)|" + "(.*(\\d|\\)|\\()\\(.*)|" + "(^[^\\d|(].*)|" + "(.*[^RrLlCc|)]$)|";

            final Pattern PT = Pattern.compile(regex);
            final Matcher MT = PT.matcher(INPUT);

            gueltig = !MT.matches();

            if (!gueltig) errorMsg += "Etwas stimmt nicht mit der Gleichung.";
        }

        // Ausgeben, ob Zeichenkette gueltig ist
        if (gueltig) {
            String regex = "(.*\\d[RrLlCc].*)";

            final Pattern PT = Pattern.compile(regex);
            final Matcher MT = PT.matcher(INPUT);
            gueltig = MT.matches();
        }
        if (!gueltig) errorMsg += "Etwas stimmt nicht mit der Gleichung.";

        if (gueltig) {
            System.out.println(("Die Zeichenkette ist gueltig."));
        } else {
            System.out.println(("Die Zeichenkette ist ungueltig.\n" + errorMsg + "\n"));
        }
        return gueltig;
    }

    /**
     * Damit werden die passenden ArrayList mit den Werten der Widerstände hinzugefügt
     *
     * @param INPUT Zeichenkette
     */
    public boolean arrayListFuellen(final String INPUT) {
        ArrayList<Double> tempParallelREArrayList = new ArrayList<>();
        ArrayList<Double> tempParallelIMArrayList = new ArrayList<>();
        WechselStromKreis.REIHEN_RE_ARRAYLIST.clear();
        WechselStromKreis.REIHEN_IM_ARRAYLIST.clear();
        WechselStromKreis.PARALLEL_RE_ARRAYLIST.clear();
        WechselStromKreis.PARALLEL_IM_ARRAYLIST.clear();
        double XL, XC;

        StringBuilder zahl = new StringBuilder(); // alternative zahl = ""; zahl += c
        boolean gueltig = true;

        for (int i = 0; i < INPUT.length(); i++) {

            char c = INPUT.charAt(i);
            char current = ' ';
            if (i > 0) current = INPUT.charAt(i - 1);

            if (Character.isDigit(c) || c == '.') {
                zahl.append(c);
            }
            if (c == '+' || c == '/' || c == '(' || c == ')' || i == INPUT.length() - 1) {
                if (!zahl.toString().equals("")) {
                    double ri = Double.parseDouble(zahl.toString());
                    if (ri > 0.0) {
                        if (c == '+' || (c != ')' && i == INPUT.length() - 1)) {
                            if (i == INPUT.length() - 1) {
                                if (c == 'R' || c == 'r') {
                                    WechselStromKreis.REIHEN_RE_ARRAYLIST.add(ri);
                                } else if (c == 'L' || c == 'l') {
                                    XL = 2 * PI * WechselStromKreis.f * ri * pow(10, -3);
                                    WechselStromKreis.REIHEN_IM_ARRAYLIST.add(XL);
                                } else if (c == 'C' || c == 'c') {
                                    XC = -1 / (2 * PI * WechselStromKreis.f * ri * pow(10, -6));
                                    WechselStromKreis.REIHEN_IM_ARRAYLIST.add(XC);
                                } else {
                                    System.out.println("Widerstandtyp fehlt, bitte R, L oder C nach der Zahl eingeben.");
                                    gueltig = false;
                                }

                            } else {
                                if (current == 'R' || current == 'r') {
                                    WechselStromKreis.REIHEN_RE_ARRAYLIST.add(ri);
                                } else if (current == 'L' || current == 'l') {
                                    XL = 2 * PI * WechselStromKreis.f * ri * pow(10, -3);
                                    WechselStromKreis.REIHEN_IM_ARRAYLIST.add(XL);
                                } else if (current == 'C' || current == 'c') {
                                    XC = -1 / ((2 * PI * WechselStromKreis.f * ri) * pow(10, -6));
                                    WechselStromKreis.REIHEN_IM_ARRAYLIST.add(XC);
                                } else {
                                    System.out.println("Widerstandtyp fehlt, bitte R, L oder C nach der Zahl eingeben.");
                                    gueltig = false;
                                }
                            }

                        } else if (c == '/' || c == ')') {
                            if (current == 'R' || current == 'r') {
                                tempParallelREArrayList.add(ri);
                            } else if (current == 'L' || current == 'l') {
                                XL = 2 * PI * WechselStromKreis.f * ri * pow(10, -3);
                                tempParallelIMArrayList.add(XL);
                            } else if (current == 'C' || current == 'c') {
                                XC = -1 / ((2 * PI * WechselStromKreis.f * ri) * pow(10, -6));
                                tempParallelIMArrayList.add(XC);
                            } else {
                                System.out.println("Widerstandtyp fehlt, bitte R, L oder C nach der Zahl eingeben.");
                                gueltig = false;
                            }

                            if (c == ')') {
                                WechselStromKreis.PARALLEL_RE_ARRAYLIST.add(tempParallelREArrayList);
                                WechselStromKreis.PARALLEL_IM_ARRAYLIST.add(tempParallelIMArrayList);
                                tempParallelREArrayList = new ArrayList<>();
                                tempParallelIMArrayList = new ArrayList<>();
                            }
                        }
                    } else {
                        System.out.println("Widerstand soll groesser als null.");
                        gueltig = false;
                    }
                }
                zahl = new StringBuilder();
            } else if (!Character.isDigit(c) && c != '.') {
                if (c != 'R' && c != 'r' && c != 'L' && c != 'l' && c != 'C' && c != 'c') {
                    System.out.println("Falsche Eingabe, versuchen Sie bitten nochmal.");
                    gueltig = false;
                    break;
                }
            }
        }
        return gueltig;
    }

    /**
     * den Wert von der Spannungsquelle einlesen
     *
     * @param STR Zeichenkette
     */
    public void spannungsQuelleEinlesen(final String STR) {
        scan = new Scanner(System.in);
        System.out.println(STR);
        double uq;
        try {
            uq = scan.nextDouble();
            while (uq <= 0.0) {
                System.out.println("Der Wert soll groeßer 0 sein");
                uq = scan.nextDouble();
            }
            setUq(uq);
        } catch (InputMismatchException e) {
            System.out.println("Das ist keine Zahl gewesen!");
        }

    }

    /**
     * den Wert von der Spannungsquelle einlesen
     *
     * @param STR Zeichenkette
     */
    public void frequenzEinlesen(final String STR) {
        scan = new Scanner(System.in);
        System.out.println(STR);
        double f;
        try {
            f = scan.nextDouble();
            while (f <= 0.0) {
                System.out.println("Der Wert soll groeßer 0 sein");
                f = scan.nextDouble();
            }
            setF(f);
            zeichenKetteEinlesen("Geben Sie bitte die Gleichung aller Widerstände in Ohm, Kapazitäten in Mikrofarad, Induktivitäten in Millihenry, '__+__' für Reihen und '(__/__)' für Parallel" + "\nz. B. => 10R + 20L + ( 30R / 40L ) ");
        } catch (InputMismatchException e) {
            System.out.println("Das ist keine Zahl gewesen!");
        }
    }

    // SETTER
    public void setF(final double f) {
        WechselStromKreis.f = f;
    }

    public void setUq(final double UQ) {
        WechselStromKreis.uq = UQ;
    }
}

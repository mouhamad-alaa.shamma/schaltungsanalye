public class Main {

    public static void main(String[] args) {

        final DC_UPN DCUPNEINLESEN = new DC_UPN();
        final ACZeichenKetteEinlesen ACEINLESEN = new ACZeichenKetteEinlesen();

        int wahl = DCUPNEINLESEN.Auswahl("Geben Sie '1' für Gleichstrom oder '2' für Wechselstrom:");
        if (wahl == 1) {
            System.out.println("-- Gleichstrom --");
            DCUPNEINLESEN.spannungsQuelleEinlesen("Bitte den Wert für die Spannungsquelle in Volt eingeben:");

        } else if (wahl == 2) {
            System.out.println("-- Wechselstrom --");
            ACEINLESEN.frequenzEinlesen("Geben Sie den Wert von der Frequenz in Hz ein:");
        }
    }

    public static double round(final double ZAHL) {
        return Math.round(ZAHL * 100.0) / 100.0;
    }
}


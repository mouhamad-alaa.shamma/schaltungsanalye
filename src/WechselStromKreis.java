import java.util.ArrayList;

public class WechselStromKreis {

    final static ArrayList<Double> REIHEN_RE_ARRAYLIST = new ArrayList<>();
    final static ArrayList<Double> REIHEN_IM_ARRAYLIST = new ArrayList<>();
    final static ArrayList<ArrayList<Double>> PARALLEL_RE_ARRAYLIST = new ArrayList<>();
    final static ArrayList<ArrayList<Double>> PARALLEL_IM_ARRAYLIST = new ArrayList<>();
    static final ArrayList<Complex> reihen = new ArrayList<>();
    static final ArrayList<ArrayList<Complex>> parallel = new ArrayList<>();
    static ArrayList<Complex> ersp = new ArrayList<>(); // ersatz fuer Parallel

    static double uq;
    static double f;
    private static double zGesamt;
    private static Complex zgesamt;

    public static void berechne() {

        Complex Uq = new Complex();
        Uq.setReal(uq);
        Uq.setImage(0);
        Complex igesamt;
        igesamt = Complex.div(Uq, zgesamt);

        double iGesamt = gesamtI();
        double WirkLeistung = getUq() * gesamtI() * Math.cos(Math.toRadians(zgesamt.Phasenverschiebung));
        double BlindLeistung = getUq() * gesamtI() * Math.sin(Math.toRadians(zgesamt.Phasenverschiebung));
        double ScheinLeistung = getUq() * gesamtI();

        System.out.println(" -------------------------\n" +
                "GesamtImpedanzen = " + Main.round(zGesamt) + " Ohm" +
                "\nGesamtstrom = " + Main.round(iGesamt) + " Ampere" +
                "\nSpannungsquelle = " + getUq() + " Volt" +
                "\nFrequenz = " + getf() + " Hz" +
                "\n\nWirkLeistung = " + Main.round(WirkLeistung) + " Watt" +
                "\nBlindLeistung = " + Main.round(BlindLeistung) + " VAR" +
                "\nScheinLeistung = " + Main.round(ScheinLeistung) + " VAR\n");

        for (double r : REIHEN_RE_ARRAYLIST) {
            Complex x = new Complex();
            x.setReal(r);
            x.setImage(0);
            reihen.add(x);
        }

        for (double lc : REIHEN_IM_ARRAYLIST) {
            Complex x = new Complex();
            x.setReal(0);
            x.setImage(lc);
            reihen.add(x);
        }

        for (Complex complex : reihen) {
            Complex U;
            U = Complex.mul(complex, igesamt);
            complex.setPhasenverschiebung(Math.toDegrees(Math.atan(U.image / U.real)));
        }
        for (int i = 0; i < PARALLEL_RE_ARRAYLIST.size(); i++) {
            ArrayList<Double> doubles = PARALLEL_RE_ARRAYLIST.get(i);
            ArrayList<Complex> prs = new ArrayList<>();
            for (double pr : doubles) {
                Complex x = new Complex();
                x.setReal(pr);
                x.setImage(0);
                prs.add(x);
            }
            parallel.add(prs);
            doubles = PARALLEL_IM_ARRAYLIST.get(i);

            for (double plc : doubles) {
                Complex x = new Complex();
                x.setReal(0);
                x.setImage(plc);
                parallel.get(i).add(x);
            }

        }

        for (int i = 0; i < parallel.size(); i++) {
            Complex u;
            u = Complex.mul(igesamt, ersp.get(i));
            for (int j = 0; j < parallel.get(i).size(); j++) {
                Complex I;
                I = Complex.div(u, parallel.get(i).get(j));
                parallel.get(i).get(j).setPhasenverschiebung(Math.toDegrees(Math.atan(I.image / I.real)));
            }

        }

        int ZaehlerR = 1;
        int ZaehlerL = 1;
        int ZaehlerC = 1;

        for (int i = 0; i < reihen.size(); i++) {

            reihen.get(i).setStrom(iGesamt);
            double spannung = iGesamt * reihen.get(i).getBetrag();
            reihen.get(i).setSpannung(spannung);
            if (reihen.get(i).image == 0 && reihen.get(i).real != 0) {
                reihen.get(i).setwNum(ZaehlerR);
                ZaehlerR++;
            }
            if (reihen.get(i).real == 0 && reihen.get(i).image > 0) {
                reihen.get(i).setwNum(ZaehlerL);
                ZaehlerL++;
            }
            if (reihen.get(i).real == 0 && reihen.get(i).image < 0) {
                reihen.get(i).setwNum(ZaehlerC);
                ZaehlerC++;
            }
        }

        for (int i = 0; i < parallel.size(); i++) {

            double spannung = iGesamt * ersp.get(i).getBetrag();
            for (int j = 0; j < parallel.get(i).size(); j++) {
                parallel.get(i).get(j).setSpannung(spannung);
                double storm = spannung / parallel.get(i).get(j).getBetrag();
                parallel.get(i).get(j).setStrom(storm);
                if (parallel.get(i).get(j).image == 0 && parallel.get(i).get(j).real != 0) {
                    parallel.get(i).get(j).setwNum(ZaehlerR);
                    ZaehlerR++;
                }
                if (parallel.get(i).get(j).real == 0 && parallel.get(i).get(j).image > 0) {
                    parallel.get(i).get(j).setwNum(ZaehlerL);
                    ZaehlerL++;
                }
                if (parallel.get(i).get(j).real == 0 && parallel.get(i).get(j).image < 0) {
                    parallel.get(i).get(j).setwNum(ZaehlerC);
                    ZaehlerC++;
                }
            }
        }

        if (!reihen.isEmpty())
            System.out.println(reihen);
        if (!parallel.isEmpty())
            System.out.println(parallel);

    }

    public static void gesamtZ() {

        double R = 0;
        double L = 0;
        double C = 0;
        double gesamtBetrag;

        Complex Zgesamt = new Complex();
        Zgesamt.setReal(0);
        Zgesamt.setImage(0);
        Complex ersR = new Complex();   //ersatz fuer Reihen
        ArrayList<Complex> ersP = new ArrayList<>();
        ArrayList<Double> ersPR = new ArrayList<>(); //ersatz fuer Parallel Widersatnd
        ArrayList<Double> ersPL = new ArrayList<>(); //ersatz fuer Parallel Induktivitaet
        ArrayList<Double> ersPC = new ArrayList<>(); //ersatz fuer Parallel Kapazitaet

        for (double real : REIHEN_RE_ARRAYLIST) {
            ersR.real += real;
        }

        for (double image : REIHEN_IM_ARRAYLIST) {
            ersR.image += image;
        }

        ersR.setPhasenverschiebung(Math.toDegrees(Math.atan(ersR.image / ersR.real)));
        for (ArrayList<Double> doubles : PARALLEL_RE_ARRAYLIST) {
            for (double k : doubles) {
                R += k;
            }
            ersPR.add(R);
        }
        for (ArrayList<Double> doubles : PARALLEL_IM_ARRAYLIST) {
            for (double k : doubles) {
                if (k > 0) {
                    L += k;
                } else {
                    C += k;
                }
            }

            ersPL.add(L);
            ersPC.add(C);
        }
        for (int i = 0; i < PARALLEL_RE_ARRAYLIST.size(); i++) {
            if (ersPL.get(i) != 0 && ersPC.get(i) == 0) {
                ersP.add(Parallel.RL_Schaltung(ersPR.get(i), ersPL.get(i)));
                ersP.get(i).setPhasenverschiebung(Math.toDegrees(Math.atan(ersPR.get(i) / ersPL.get(i))));
            } else if (ersPL.get(i) == 0 && ersPC.get(i) != 0) {
                ersP.add(Parallel.RC_Schaltung(ersPR.get(i), ersPC.get(i)));
                ersP.get(i).setPhasenverschiebung(Math.toDegrees(Math.atan(ersPR.get(i) / ersPC.get(i))));
            } else if (ersPL.get(i) != 0 && ersPC.get(i) != 0) {
                ersP.add(Parallel.RLC_Schaltung(ersPR.get(i), ersPL.get(i), ersPC.get(i)));
                ersP.get(i).setPhasenverschiebung(Math.toDegrees(Math.atan(ersPR.get(i) / (ersPC.get(i) + ersPL.get(i)))));
            }
        }
        for (Complex complex : ersP) {
            Zgesamt = Complex.add(Zgesamt, complex);
        }

        Zgesamt = Complex.add(Zgesamt, ersR);
        zgesamt = Zgesamt;
        gesamtBetrag = Zgesamt.getBetrag();
        if (ersR.real == 0 && ersR.image == 0 && ersP.size() == 1) {
            Zgesamt.setPhasenverschiebung(ersP.get(0).Phasenverschiebung);
        } else {
            Zgesamt.setPhasenverschiebung(Math.toDegrees(Math.atan(Zgesamt.image / Zgesamt.real)));
        }

        ersp = ersP;
        zGesamt = gesamtBetrag;

    }

    public static double gesamtI() {
        return (getUq() / zGesamt);
    }

    public static double getUq() {
        return uq;
    }

    public static double getf() {
        return f;
    }
}


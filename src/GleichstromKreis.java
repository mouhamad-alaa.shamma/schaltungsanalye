public class GleichstromKreis {

    public final double widerstand;
    public boolean istEingabe;
    public double strom;
    public double spannung;
    public GleichstromKreis Link;
    public GleichstromKreis Recht;

    public char operator;

    public GleichstromKreis(double r) {
        this.widerstand = r;
    }

    public static void istBlatt(GleichstromKreis root) {

        if (root == null) {
            return;
        }
        root.istEingabe = root.Link == null && root.Recht == null;
        istBlatt(root.Link);
        istBlatt(root.Recht);
    }

    public static void ausgeben(GleichstromKreis root) {

        if (root == null) {
            return;
        }
        if (root.istEingabe) {
            System.out.println(root);
        }
        ausgeben(root.Link);
        ausgeben(root.Recht);
    }

    public void trans() {

        if (!this.istEingabe) {
            if (this.operator == '+') {
                if (this.strom == 0) {
                    this.strom = this.spannung / this.widerstand;
                }
                this.Link.strom = this.strom;
                this.Recht.strom = this.strom;
            } else if (this.operator == '/') {
                if (this.spannung == 0) {
                    this.spannung = this.strom * this.widerstand;
                }
                this.Link.spannung = this.spannung;
                this.Recht.spannung = this.spannung;
            }
            this.Link.trans();
            this.Recht.trans();
        }
    }

    public void berechnen() {

        if (!this.istEingabe) {
            this.Link.berechnen();
            this.Recht.berechnen();
        }
        if (this.strom == 0) {
            this.strom = this.spannung / this.widerstand;
        }
        if (this.spannung == 0) {
            this.spannung = this.strom * this.widerstand;
        }
    }

    @Override
    public String toString() {
        return "R" + " = " + widerstand + " Ohm\t\t"
                + "U" + " = " + Main.round(spannung) + " Volt\t\t"
                + "I" + " = " + Main.round(strom) + " Ampere\t\t"
                + "P" + " = " + Main.round(spannung * strom) + " Watt";
    }
}




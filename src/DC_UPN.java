import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DC_UPN {

    static double uq;
    private static double rGesamt;
    private static Scanner scan;

    public static String toUpn(String str) {

        Stack<String> stack = new Stack<>();
        String[] array = str.split(" ");
        StringBuilder strbui = new StringBuilder();

        for (String current : array) {
            if ((current.charAt(0) == '+') || (current.charAt(0) == '/') || (current.charAt(0) == '(') || (current.charAt(0) == ')')) {
                if (stack.isEmpty()) {
                    stack.push(current);
                } else {
                    if (current.equals("+") && stack.lastElement().equals("+") || current.equals("/") && stack.lastElement().equals("/")) {
                        strbui.append(stack.pop()).append(" ");
                        stack.push(current);
                    } else if (current.equals("/") && stack.lastElement().equals("+") || current.equals("/") && stack.lastElement().equals("(")) {
                        stack.push(current);
                    } else if (current.equals("+") && stack.lastElement().equals("/")) {
                        while (!stack.isEmpty()) {
                            strbui.append(stack.pop()).append(" ");
                        }
                        stack.push(current);
                    } else if (current.equals(")")) {
                        while (!stack.lastElement().equals("(")) {
                            strbui.append(stack.pop()).append(" ");
                        }
                        stack.pop();
                    } else {
                        stack.push(current);
                    }
                }
            } else {
                strbui.append(current).append(" ");
            }
        }

        while (!stack.isEmpty()) {
            strbui.append(stack.pop()).append(" ");
        }

        return strbui.toString();
    }

    public static void berechne(String str) {

        System.out.println(str);
        scan = new Scanner(System.in);

        String input = scan.nextLine();
        Stack<Double> stack = new Stack<>();
        Stack<GleichstromKreis> stackElement = new Stack<>();

        boolean gueltig = eingabePruefen(input.replaceAll("\\s+", ""));

        if (gueltig) {
            String[] eingabeArray = toUpn(input).split(" "); //Erzeuge ein String-Array
            String current;
            double operand1, operand2;

            // Alle Arrayelemente durchlaufen
            for (int i = 0; i < eingabeArray.length; i++) {
                current = eingabeArray[i];

                //Falls aktuelles Arrayelement eine Zahl ist, lege sie auf dem Stack ab.
                if (!isOperator(current)) {
                    stack.push(Double.valueOf(current));
                    stackElement.push(new GleichstromKreis(Double.parseDouble(current)));
                } else {
                    if (!stack.isEmpty()) {
                        operand1 = stack.pop();//Entnehme den ersten Operanden vom Stack
                        GleichstromKreis op1 = stackElement.pop();
                        if (!stack.isEmpty()) {
                            operand2 = stack.pop();    //Entnehme den zweiten Operanden vom Stack
                            GleichstromKreis op2 = stackElement.pop();
                            // Fuehre die Berechnung abhaengig vom Operator durch
                            double result = ersatzWiderstand(current, operand1, operand2);
                            stack.push(result);
                            GleichstromKreis op3 = new GleichstromKreis(result);
                            if (current.equals("+")) {
                                op3.operator = '+';
                            } else {
                                op3.operator = '/';
                            }
                            op3.Link = op1;
                            op3.Recht = op2;
                            stackElement.push(op3);
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
        }

        //Ausgabe des Ergebnisses
        if (!gueltig) {
            System.out.println("Ein Fehler ist aufgetreten!\n");
        } else {
            setRGesamt(stack.pop());
            System.out.println("-----------------------------\n" +
                    "Gesamtwiderstand = " + Main.round(getRGesamt()) + " Ohm\n" +
                    "Gesamtstrom = " + Main.round(gesamtI()) + " Ampere\n" +
                    "Spannungsquelle = " + getUq() + " Volt\n");

            GleichstromKreis root = stackElement.pop();
            GleichstromKreis.istBlatt(root);
            root.strom = gesamtI();
            root.spannung = getUq();
            root.trans();
            root.berechnen();
            GleichstromKreis.ausgeben(root);
        }
    }

    static boolean isOperator(String str) {
        return (str.equalsIgnoreCase("+") || str.equalsIgnoreCase("/"));
    }

    static double ersatzWiderstand(String operator, double op1, double op2) {
        double result = 0;
        switch (operator) {
            case "+":
                result = op1 + op2;
                break;
            case "/":
                result = 1 / (1 / op1 + 1 / op2);
                break;
        }
        return result;
    }

    /**
     * Hier wird die Zeichenkette geprüft, falls mehr oder weniger schließenden Klammer als Öffnenden
     * Außerdem wird geprüft, ob die Zeichenkette Buchstaben enthält und ob zwischen den Klammer '+' statt '/' eingegeben wurde
     *
     * @param INPUT Zeichenkette
     */
    public static boolean eingabePruefen(final String INPUT) {

        String errorMsg = "";

        Stack<Character> klammerStack = new Stack<>();
        char current, top;
        boolean gueltig = true; // Setze die Gueltigkeits-Variable erstmal auf true

        // durch Zeichen des Strings iterieren
        for (int i = 0; i < INPUT.length(); i++) {
            current = INPUT.charAt(i);

            if (Character.isLetter(current)) {
                errorMsg = "Es soll keinen Buchstabe eingegeben werden.";
                gueltig = false;
                break;
            }

            if (Character.isDigit(current)) continue;

            if (current == '(') {
                klammerStack.push(current);
            } else if (current == ')') {
                //Fall 1: mehr schliessende Klammern als oeffnende.
                //Falls eine schliessende Klammer vorhanden ist, obwohl der Stack leer ist, ist die Zeichenkette ungültig
                if (klammerStack.isEmpty()) {
                    errorMsg = "Fehler: Mehr schliessende Klammern als oeffnende.";
                    gueltig = false;
                } else {
                    // oberste Klammer vom Stack nehmen
                    top = klammerStack.pop();

                    //Fall 2: Falls die aktuelle Klammer nicht zur obersten Klammer auf dem Stack passt, ist die Zeichenkette ungültig
                    if (top != '(') {
                        errorMsg = "Fehler: Die aktuelle schliessende Klammer passt\nnicht zur obersten Klammer auf dem Stack.";
                        gueltig = false;
                    }
                }
            } else if (current != '+' && current != '/' && current != '.') {
                errorMsg = "Fehler: Die Zeichen '" + current + "' ist unzulaessig";
                gueltig = false;
            }
        }

        //Fall 3: mehr oeffnende Klammern als Schliessende.
        //Falls keine schliessenden Klammern vorhanden sind, obwohl der Stack nicht leer ist, ist die Zeichenkette ungültig
        if (!klammerStack.isEmpty()) {
            errorMsg = "Fehler: Es sind keine weiteren schliessenden Klammern vorhanden.";
            gueltig = false;
        }

        /*
         * Hier werden 6 Arten von Fehleingaben durch reguläre Ausdrücke ausgeschlossen
         * 1. Keine Zahl nach der schliessenden Klammer.                                     z.B. (2+3)3.
         * 2. Kein Operator oder schliessende Klammer nach einem Operator.                   z.B. (2++3), (2+).
         * 3. Kein Operator nach oeffnenden Klammer.                                         z.B. (+2+3).
         * 4. Die Zeichenkette muss mit einer oeffnenden Klammer oder einer Zahl beginnen.   z.B. +8+3
         * 5. muss mit einer Zahl oder einer schliessenden Klammer enden.                    z.B. 3+6+
         */
        if (gueltig) {
            String regex =
                    "(.*\\)\\d.*)|"
                            + "(.*[+/]([+/]|\\)).*)|"
                            + "(.*\\([+/].*)|"
                            + "(^[^\\d|(].*)|"
                            + "(.*[^\\d|)]$)|";

            final Pattern PT = Pattern.compile(regex);
            final Matcher MT = PT.matcher(INPUT);

            gueltig = !MT.matches();

            if (!gueltig) errorMsg += "Etwas stimmt nicht mit der Gleichung.";
        }

        // Ausgeben, ob Zeichenkette gueltig ist
        if (gueltig) {
            System.out.println(("Die Zeichenkette ist gueltig."));
        } else {
            System.out.println(("Die Zeichenkette ist ungueltig.\n" + errorMsg + "\n"));
        }
        return gueltig;
    }

    /**
     * Berechnung der Gesamtstrom
     */
    public static double gesamtI() {
        return (getUq() / getRGesamt());
    }

    // GETTER & SETTER
    public static double getUq() {
        return uq;
    }

    public void setUq(final double UQ) {
        uq = UQ;
    }

    public static double getRGesamt() {
        return rGesamt;
    }

    public static void setRGesamt(final double R_GESAMT) {
        rGesamt = R_GESAMT;
    }

    /**
     * Auswahl zwischen Gleichstrom und Wechselstrom
     *
     * @param STR Zeichenkette
     */
    public int Auswahl(final String STR) {
        scan = new Scanner(System.in);
        int input;

        while (true) {
            System.out.println(STR);
            try {
                input = scan.nextInt();
                if (input == 1) {
                    return 1;
                } else if (input == 2) {
                    return 2;
                } else if (input == 3) {
                    return 3;
                }
            } catch (InputMismatchException e) {
                System.out.println("Das ist keine Zahl gewesen!");
                break;
            }
        }
        return 0;
    }

    public void spannungsQuelleEinlesen(final String STR) {
        scan = new Scanner(System.in);
        System.out.println(STR);
        double uq;
        try {
            uq = scan.nextDouble();
            while (uq <= 0.0) {
                System.out.println("Der Wert soll groeßer 0 sein");
                uq = scan.nextDouble();
            }
            setUq(uq);
            berechne("Geben Sie bitte die Gleichung aller Widerstände in Ohm ein, ' __ + __ ' für Reihen- und '( __ / __ )' für Parallel-Schaltungen" +
                    "\nz. B. => 10 + 20 + ( 30 / 40 )");
        } catch (InputMismatchException e) {
            System.out.println("Das ist keine Zahl gewesen!");
        }
    }

}

public class Parallel {

    public static Complex RL_Schaltung(double R, double XL) {

        double real = (XL * XL * R) / (R * R + XL * XL);
        double image = (R * R * XL) / (R * R + XL * XL);
        Complex Z = new Complex();
        Z.setReal(real);
        Z.setImage(image);
        return Z;
    }

    public static Complex RC_Schaltung(double R, double Xc) {

        double XC = -Xc;
        double real = (XC * XC * R) / (R * R + XC * XC);
        double image = -(R * R * XC) / (R * R + XC * XC);
        Complex Z = new Complex();
        Z.setReal(real);
        Z.setImage(image);
        return Z;
    }

    public static Complex RLC_Schaltung(double R, double XL, double Xc) {
        double XC = -Xc;
        double real = (XL * XL * XC * XC * R) / ((XL * XL * XC * XC) + (R * R * (XL - XC) * (XL - XC)));
        double image = -(R * R * XL * XC) * (XL - XC) / ((XL * XL * XC * XC) + (R * R * (XL - XC) * (XL - XC)));
        Complex Z = new Complex();
        Z.setReal(real);
        Z.setImage(image);
        return Z;
    }

}

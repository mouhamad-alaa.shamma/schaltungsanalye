public class Complex {

    double real;
    double image;
    double betrag;
    double spannung;
    double strom;
    int wNum;
    double Phasenverschiebung;

    Complex() {
    }

    public static Complex add(Complex a, Complex b) {
        double real1 = a.real;
        double image1 = a.image;
        double real2 = b.real;
        double image2 = b.image;
        double newReal = real1 + real2;
        double newImage = image1 + image2;
        Complex result = new Complex();
        result.setReal(newReal);
        result.setImage(newImage);
        return result;
    }

    public static Complex mul(Complex a, Complex b) {
        double real2 = a.real;
        double image2 = a.image;
        double real3 = b.real;
        double image3 = b.image;
        double newReal = real3 * real2 - image3 * image2;
        double newImage = image3 * real2 + real3 * image2;
        Complex result = new Complex();
        result.setReal(newReal);
        result.setImage(newImage);
        return result;
    }

    public static Complex div(Complex a, Complex b) {
        double real2 = a.real;
        double image2 = a.image;
        double real3 = b.real;
        double image3 = b.image;
        double newReal = (real2 * real3 + image2 * image3) / (real3 * real3 + image3 * image3);
        double newImage = (image2 * real3 - real2 * image3) / (real3 * real3 + image3 * image3);
        Complex result = new Complex();
        result.setReal(newReal);
        result.setImage(newImage);
        return result;
    }

    public double getBetrag() {
        this.betrag = Math.sqrt(real * real + image * image);
        return betrag;
    }

    public void setReal(final double REAL) {
        this.real = REAL;
    }

    public void setImage(final double IMAGE) {
        this.image = IMAGE;
    }

    public void setSpannung(final double SPANNUNG) {
        this.spannung = SPANNUNG;
    }

    public void setStrom(final double STROM) {
        this.strom = STROM;
    }

    public void setwNum(int num) {
        this.wNum = num;
    }

    public void setPhasenverschiebung(final double winkel) {
        this.Phasenverschiebung = winkel;
    }

    @Override
    public String toString() {
        if (this.image == 0) {
            return "R" + wNum + " = " + betrag + " Ohm\t\t"
                    + "Ur" + wNum + " = " + Main.round(spannung) + " Volt\t\t"
                    + "Ir" + wNum + " = " + Main.round(strom) + " Ampere\t\t"
                    + "φr" + wNum + " = " + Main.round(Phasenverschiebung) + " grad\n"
                    ;
        } else if (this.real == 0 && this.image > 0) {
            return "XL" + wNum + " = " + Main.round(betrag) + " Ohm"
                    + "\t\tUl" + wNum + " = " + Main.round(spannung) + " Volt"
                    + "\t\tIl" + wNum + " = " + Main.round(strom) + " Ampere"
                    + "\tφl" + wNum + " = " + Main.round(Phasenverschiebung) + " grad\n"
                    ;
        } else if (this.real == 0 && this.image < 0) {
            return "XC" + wNum + " = " + Main.round(betrag) + " Ohm"
                    + "\t\tUc" + wNum + " = " + Main.round(spannung) + " Volt"
                    + "\t\tIc" + wNum + " = " + Main.round(strom) + " Ampere"
                    + "\tφc" + wNum + " = " + Main.round(Phasenverschiebung) + " grad\n"
                    ;
        } else {
            return "Inpedanz" + wNum + " = " + Main.round(betrag) + " Ohm"
                    + "\tU" + wNum + " = " + Main.round(spannung) + " Volt"
                    + "\tI" + wNum + " = " + Main.round(strom) + " Ampere"
                    + "\tφ" + wNum + " = " + Main.round(Phasenverschiebung) + " grad\n"
                    ;
        }
    }
}
